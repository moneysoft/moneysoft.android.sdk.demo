package com.moneysoft.moneysoftdemoapp.controllers

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import com.moneysoft.moneysoftdemoapp.R
import com.moneysoft.moneysoftdemoapp.components.EndpointAdapter
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

abstract class BaseActivity : AppCompatActivity() {
	internal lateinit var endpointList: ListView
	internal var endpoints: List<ApiEndpoint> = emptyList()
	internal lateinit var endpointAdapter: EndpointAdapter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.base_view)

		ServiceManager.rootActivity = this
		endpointList = findViewById(R.id.endpointList)
		endpointAdapter = EndpointAdapter(this, R.layout.api_endpoint_cell)
		endpointList.adapter = endpointAdapter
		endpointList.setOnItemClickListener {
			_: AdapterView<*>, _: View, position: Int, _: Long ->
				val selectedEndpoint = endpoints[position]

				println(selectedEndpoint.name)
				selectedEndpoint.action()
		}

		acceptSelfSignedCertificates()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		// Inflate the menu; this adds items to the action bar if it is present.
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return when (item.itemId) {
			R.id.action_settings -> true
			else -> super.onOptionsItemSelected(item)
		}
	}

	fun displayEndpoints() {
		runOnUiThread {
			endpoints = ServiceManager.activeService?.getEndpoints() ?: emptyList()
			endpointAdapter.clear()
			endpointAdapter.addAll(endpoints.toMutableList())
		}
	}

	override fun onBackPressed() {
		ServiceManager.activeService?.back()
	}

	abstract fun createEndpoints()

	private fun acceptSelfSignedCertificates() {
		val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
			override fun getAcceptedIssuers(): Array<X509Certificate> {
				return arrayOf()
			}

			@SuppressLint("TrustAllX509TrustManager")
			override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) {
			}

			@SuppressLint("TrustAllX509TrustManager")
			override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) {
			}
		})

		val sc = SSLContext.getInstance("SSL")
		sc.init(null, trustAllCerts, SecureRandom())
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
		HttpsURLConnection.setDefaultHostnameVerifier { arg0, arg1 -> true }
	}
}
