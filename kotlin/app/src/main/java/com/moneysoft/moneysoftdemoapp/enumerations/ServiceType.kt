package com.moneysoft.moneysoftdemoapp.enumerations

enum class ServiceType {
	AUTHORISATION,
	MONEYSOFT,
	FINANCIAL,
	TRANSACTION,
	USER
}