package com.moneysoft.moneysoftdemoapp.ui.dialogs

import android.app.Activity
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.moneysoft.moneysoftdemoapp.R

open class PopupDialog(private val activity: Activity) {
	var header: String = ""
	protected var dialog: AlertDialog? = null
	protected lateinit var layout: View
	protected var cancel: (() -> Unit)? = null
	protected var success: (() -> Unit)? = null

	fun show(layoutResource: Int) {
		dialog = AlertDialog.Builder(activity).create()
		layout = LayoutInflater.from(activity).inflate(layoutResource, null)

		updateHeader()
		bindCancel()
		bindSuccess()
	}

	fun hide() {
		dialog?.dismiss()
	}

	fun onCancel(cancel: (() -> Unit)? = null) {
		this.cancel = cancel
	}

	fun onSuccess(success: (() -> Unit)? = null) {
		this.success = success
	}

	private fun updateHeader() {
		val headerTxtView = layout.findViewById<TextView>(R.id.dialog_header)

		headerTxtView.text = header
	}

	private fun bindCancel() {
		val cancelBtn = layout.findViewById<Button>(R.id.dialog_cancel_button)

		cancelBtn.setOnClickListener {
			hide()
			cancel?.invoke()
		}
	}

	private fun bindSuccess() {
		val successBtn = layout.findViewById<Button>(R.id.dialog_ok_button)

		successBtn.setOnClickListener {
			hide()
			success?.invoke()
		}
	}
}