package com.moneysoft.moneysoftdemoapp.services.moneysoft

import com.moneysoft.moneysoftdemoapp.enumerations.AuthorisationEndpoint
import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.sdk.api.MoneysoftApi
import com.moneysoft.sdk.common.models.AuthenticationModel
import com.moneysoft.sdk.listeners.ApiListener

class AuthorisationService: IService {
	fun login() {
		val msApi = MoneysoftApi()
		val authenticationListener = ApiListener<AuthenticationModel>(successHandler = {
			(_) ->
				println("Success")
				ServiceManager.activeService = ServiceFactory().create(ServiceType.MONEYSOFT)
		}, errorHandler = {
			println("Error")
		})

//		msApi.user().login(username = "cheq_02@usecheq.com",
//		                   password = "cheq02!",
//		                   listener = authenticationListener)
		msApi.user().login(username = "moneysoft.test2017+cheqand1@gmail.com",
		                   password = "Moneysoft11",
		                   listener = authenticationListener)
	}

	fun logout() {

	}

	override fun back() {
		// Nothing to do as we are the top-most level now
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
			ApiEndpoint(name = AuthorisationEndpoint.LOGIN.label,
			            action = {
				            login()
			            }),
			ApiEndpoint(name = AuthorisationEndpoint.LOGOUT.label,
			            action = {
				            logout()
			            })
		)
	}
}