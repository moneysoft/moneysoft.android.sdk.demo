package com.moneysoft.moneysoftdemoapp.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.moneysoft.moneysoftdemoapp.R
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint

class EndpointAdapter(context: Context, resource: Int) : ArrayAdapter<ApiEndpoint>(context, resource) {
	override fun getView(position: Int, view: View?, parent: ViewGroup): View {
		var cell = view
		
		if (cell == null) {
			cell = LayoutInflater.from(context).inflate(R.layout.api_endpoint_cell, parent, false)
		}
		
		val endpoint = getItem(position)
		
		if (endpoint != null) {
			val nameLbl = cell?.findViewById<TextView>(R.id.endpointNameLbl)
			
			if (nameLbl != null) {
				nameLbl.text = endpoint.name
			}
		}

		return cell!!
	}
}