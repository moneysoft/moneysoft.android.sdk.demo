package com.moneysoft.moneysoftdemoapp.models

data class ApiEndpoint(val name: String,
                       val action: () -> Unit) {
}