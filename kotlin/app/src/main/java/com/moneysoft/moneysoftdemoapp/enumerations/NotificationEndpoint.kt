package com.moneysoft.moneysoftdemoapp.enumerations

enum class NotificationEndpoint(val label: String) {
	REGISTER("Register"),
	ACCOUNT_REFRESH("Account Refresh"),
	PUSH("Push")
}