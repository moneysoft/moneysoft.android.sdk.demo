package com.moneysoft.moneysoftdemoapp.managers

import android.app.Activity
import com.moneysoft.moneysoftdemoapp.ui.dialogs.InputDialog

class DialogManager {
	lateinit var activity: Activity

	fun configure(activity: Activity) {
		this.activity = activity
	}

	fun createInputDialog(): InputDialog {
		return InputDialog(activity)
	}
}

