package com.moneysoft.moneysoftdemoapp.enumerations

enum class UserEndpoint(val label: String) {
	PROFILE("Get Profile"),
}