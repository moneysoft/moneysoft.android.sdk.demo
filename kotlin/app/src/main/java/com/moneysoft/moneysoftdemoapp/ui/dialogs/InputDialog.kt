package com.moneysoft.moneysoftdemoapp.ui.dialogs

import android.app.Activity
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.moneysoft.moneysoftdemoapp.R

class InputDialog(private val activity: Activity) : PopupDialog(activity) {
	val inputs: List<View>
		get() = _inputs.toList()
	private val _inputs: MutableList<View> = mutableListOf()

	fun addInput(label: String? = null,
	             component: View,
	             tag: String) {
		if (label != null) {
			val labelFld = TextView(activity)

			labelFld.text = label
			_inputs.add(labelFld)
		}

		component.tag = tag
		_inputs.add(component)
	}

	fun show() {
		super.show(layoutResource = R.layout.input_dialog)

		val fieldsLayout = layout.findViewById<LinearLayout>(R.id.content_container)

		this.inputs.forEach {
			fieldsLayout?.addView(it)
		}

		dialog?.setView(layout)
		dialog?.show()
	}
}