package com.moneysoft.moneysoftdemoapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.Button
import com.moneysoft.moneysoftdemoapp.controllers.BaseActivity
import com.moneysoft.moneysoftdemoapp.enumerations.AuthorisationEndpoint
import com.moneysoft.moneysoftdemoapp.managers.Manager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.sdk.api.*
import com.moneysoft.sdk.pfm.models.Config

class AuthorisationActivity : BaseActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		MoneysoftApi.configure(config = Config(appContext = this.applicationContext,
		                                       baseApiUrl = "https://api.cheq.moneysoft.com.au",
		                                       apiReferrer = "https://pfm.cheq.moneysoft.com.au",
		                                       isBeta = false,
		                                       isDebug = true,
		                                       providerTimeoutMinutes = 2))
//		MoneysoftApi.configure(context = this.applicationContext,
//		                       baseEndpointUrl = "https://api.beta.moneysoft.com.au",
//		                       referrerUrl = "https://localhost",
//		                       webView = findViewById(R.id.webViewMain),
//		                       beta = true,
//		                       debug = true)
		Manager.Dialogs.configure(this)

		val backBtn = findViewById<Button>(R.id.backBtn)

		if (backBtn != null) {
			backBtn.visibility = View.GONE
		}

		createEndpoints()
		displayEndpoints()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		// Inflate the menu; this adds items to the action bar if it is present.
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return when (item.itemId) {
			R.id.action_settings -> true
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun createEndpoints() {
//		for (endpoint in AuthorisationEndpoint.values()) {
//			endpoints.add(ApiEndpoint(name = endpoint.label))
//		}
	}
}
