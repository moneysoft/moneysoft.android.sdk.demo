package com.moneysoft.moneysoftdemoapp.factories

import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.moneysoftdemoapp.services.moneysoft.*

class ServiceFactory : IServiceFactory {
	override fun create(type: ServiceType): IService {
		when(type) {
			ServiceType.AUTHORISATION -> return createAuthorisationService()
			ServiceType.MONEYSOFT -> return createMoneysoftService()
			ServiceType.FINANCIAL -> return createFinancialService()
			ServiceType.TRANSACTION -> return createTransactionService()
			ServiceType.USER -> return createUserService()
		}

		throw Exception("Invalid service request...")
	}

	internal fun createAuthorisationService(): AuthorisationService {
		return AuthorisationService()
	}

	internal fun createMoneysoftService(): MoneysoftService {
		return MoneysoftService()
	}

	internal fun createFinancialService(): FinancialService {
		return FinancialService()
	}

	internal fun createTransactionService(): TransactionService {
		return TransactionService()
	}

	internal fun createUserService(): UserService {
		return UserService()
	}
}