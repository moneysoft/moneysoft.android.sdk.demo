package com.moneysoft.moneysoftdemoapp.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import com.moneysoft.moneysoftdemoapp.R

class OptionsAdapter<T>(context: Context,
                        resource: Int,
                        private val multiSelect: Boolean) : ArrayAdapter<Pair<String, T>>(context, resource) {
	private val cellResource: Int = resource

	override fun getView(position: Int, view: View?, parent: ViewGroup): View {
		var cell = view

		if (cell == null) {
			cell = LayoutInflater.from(context).inflate(cellResource, parent, false)
		}

		val item = getItem(position)

		if (item != null) {
			val nameLbl = cell?.findViewById<TextView>(R.id.nameLbl)

			nameLbl?.text = item.first

			if (multiSelect) {
				val selectChkBox = cell?.findViewById<CheckBox>(R.id.selectChkBox)

				//selectChkBox?.visibility = View.VISIBLE
			}
		}

		return cell!!
	}
}