package com.moneysoft.moneysoftdemoapp.services.moneysoft

import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.enumerations.NotificationEndpoint
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.sdk.api.MoneysoftApi
import com.moneysoft.sdk.listeners.ApiListener
import com.moneysoft.sdk.models.ApiResponseModel

class NotificationService : IService {
	fun registerDevice() {
		val msApi = MoneysoftApi()
		val tokenRegistrationListener = ApiListener<ApiResponseModel>(successHandler = {
			print("registered")
		}, errorHandler = {
			print("Failed to register")
			print(it.code)
		})

		msApi.notification().registerToken(token = "",
		                                   listener = tokenRegistrationListener)
	}

	override fun back() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.MONEYSOFT)
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
				ApiEndpoint(name = NotificationEndpoint.REGISTER.label,
				            action = {
								registerDevice()
				            }),
				ApiEndpoint(name = NotificationEndpoint.ACCOUNT_REFRESH.label,
				            action = {

				            }),
				ApiEndpoint(name = NotificationEndpoint.PUSH.label,
				            action = {

				            })
		)
	}
}