package com.moneysoft.moneysoftdemoapp.services.moneysoft

import android.widget.EditText
import com.moneysoft.moneysoftdemoapp.enumerations.FinancialEndpoint
import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.enumerations.TransactionEndpoint
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.Manager
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.sdk.api.MoneysoftApi
import com.moneysoft.sdk.listeners.ApiListListener
import com.moneysoft.sdk.models.transactions.FinancialTransactionModel
import com.moneysoft.sdk.pfm.models.transactions.TransactionCategoryGroup
import com.moneysoft.sdk.pfm.models.transactions.TransactionFilter
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.util.*

class TransactionService : IService {
	fun prepareGetTransactions() {
		val ACCOUNT_ID_TAG = "account_id"
		val START_DATE_TAG = "start_date"
		val END_DATE_TAG = "end_date"
		val PAGE_TAG = "page"
		val COUNT_TAG = "count"
		val inputDialog = Manager.Dialogs.createInputDialog()

		inputDialog.header = "Transaction Filter"
		inputDialog.addInput(label = "Account ID: ",
		                     component = EditText(Manager.Dialogs.activity),
		                     tag = ACCOUNT_ID_TAG)
		inputDialog.addInput(label = "Start Date: ",
		                     component = EditText(Manager.Dialogs.activity),
		                     tag = "start_date")
		inputDialog.addInput(label = "End Date: ",
		                     component = EditText(Manager.Dialogs.activity),
		                     tag = "end_date")
		inputDialog.addInput(label = "Page: ",
		                     component = EditText(Manager.Dialogs.activity),
		                     tag = "page")
		inputDialog.addInput(label = "Count: ",
		                     component = EditText(Manager.Dialogs.activity),
		                     tag = "count")
		inputDialog.onSuccess {
			val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
			val transactionFilterBldr = TransactionFilter.Builder()
			val taggedInputs = inputDialog.inputs.filter {
				it.tag != null
			}

			// Parse the account id
			val enteredAccount = (taggedInputs.find {
				it.tag.equals(ACCOUNT_ID_TAG)
			} as? EditText)?.text?.toString()

			if (enteredAccount != null && enteredAccount.isNotEmpty()) {
				transactionFilterBldr.account(accountId = enteredAccount.toIntOrNull()!!)
			}

			// Parse the start date
			val enteredStartDate = (taggedInputs.find {
				it.tag.equals(START_DATE_TAG)
			} as? EditText)?.text?.toString()

			if (enteredStartDate != null && enteredStartDate.isNotEmpty()) {
				val localStartDate =  LocalDate.parse(enteredStartDate, dateFormatter)

				transactionFilterBldr.startDate(date = GregorianCalendar(localStartDate.year, localStartDate.monthValue - 1, localStartDate.dayOfMonth).time)
			}

			// Parse the end date
			val enteredEndDate = (taggedInputs.find {
				it.tag.equals(END_DATE_TAG)
			} as? EditText)?.text?.toString()

			if (enteredEndDate != null && enteredEndDate.isNotEmpty()) {
				val localEndDate =  LocalDate.parse(enteredEndDate, dateFormatter)

				transactionFilterBldr.endDate(date = GregorianCalendar(localEndDate.year, localEndDate.monthValue - 1, localEndDate.dayOfMonth).time)
			}

			// Parse the page
			val enteredPage = (taggedInputs.find {
				it.tag.equals(PAGE_TAG)
			} as? EditText)?.text?.toString()

			if (enteredPage != null && enteredPage.isNotEmpty()) {
				transactionFilterBldr.offset(offset = enteredPage.toIntOrNull()!!)
			}

			// Parse the count
			val enteredCount = (taggedInputs.find {
				it.tag.equals(COUNT_TAG)
			} as? EditText)?.text?.toString()

			if (enteredCount != null && enteredCount.isNotEmpty()) {
				transactionFilterBldr.count(count = enteredCount.toIntOrNull()!!)
			}

			getTransactions(filter = transactionFilterBldr.build())
		}

		inputDialog.show()
	}

	fun getCategories() {
		val msApi = MoneysoftApi()
		val categoriesListener = ApiListListener<TransactionCategoryGroup>(successHandler = {
			for (categoryGroup in it) {
				println("Category Group: " + categoryGroup.name + " {#" + categoryGroup.colour + "}")

				for (category in categoryGroup.categoryList) {
					println("\tCategory: " + category.name)
				}
			}
		}, errorHandler = {
			println(it.code)
		})

		msApi.transactions().getCategoryGroups(listener = categoriesListener)
	}

	private fun getTransactions(filter: TransactionFilter) {
		val msApi = MoneysoftApi()
		val transactionsListener = ApiListListener<FinancialTransactionModel>(successHandler = {
			for (transaction in it) {
				println("TRANSACTION: ${transaction.name} | ${transaction.accountId} | ${transaction.date}")
			}
		}, errorHandler = {
			println(it.code)
		})

		msApi.transactions().getTransactions(filter = filter,
		                                     listener = transactionsListener)
	}

	override fun back() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.MONEYSOFT)
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
			ApiEndpoint(name = TransactionEndpoint.GET_TRANSACTIONS.label,
			            action = {
				            prepareGetTransactions()
			            }),
			ApiEndpoint(name = TransactionEndpoint.GET_CATEGORIES.label,
			            action = {
				            getCategories()
			            })
		)
	}
}