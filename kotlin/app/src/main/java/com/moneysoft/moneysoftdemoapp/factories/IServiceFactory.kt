package com.moneysoft.moneysoftdemoapp.factories

import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.services.IService

interface IServiceFactory {
	fun create(type: ServiceType): IService
}