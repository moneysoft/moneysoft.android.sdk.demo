package com.moneysoft.moneysoftdemoapp.managers

import com.moneysoft.moneysoftdemoapp.controllers.BaseActivity
import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.services.IService

object ServiceManager {
	var activeService: IService? = ServiceFactory().create(ServiceType.AUTHORISATION)
		set(value) {
			field = value
			rootActivity?.displayEndpoints()
		}

	// Not a great solution, but this is just a demo app
	var rootActivity: BaseActivity? = null
}