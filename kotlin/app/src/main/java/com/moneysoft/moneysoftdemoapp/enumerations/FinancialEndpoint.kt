package com.moneysoft.moneysoftdemoapp.enumerations

enum class FinancialEndpoint(val label: String) {
	INSTITUTIONS("Get Institutions"),
	PROMPTS("Get Prompts"),
	LINKABLE_ACCOUNTS("Get Linkable Accounts"),
	LINKABLE_ACCOUNTS_MFA("Get Linkable Accounts (MFA)"),
	LINK_ACCOUNTS("Link Accounts"),
	GET_ACCOUNT("Get Account"),
	GET_ACCOUNTS("Get Accounts"),
	REFRESH_ACCOUNT("Refresh Account (without transactions)"),
	REFRESH_ACCOUNTS("Refresh Accounts"),
	UPDATE_TRANSACTIONS("Update Transactions"),
	UNLINK_ACCOUNTS("Unlink Account(s)")
}