package com.moneysoft.moneysoftdemoapp.services.moneysoft

import com.moneysoft.moneysoftdemoapp.enumerations.MoneysoftEndpoint
import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService

class MoneysoftService: IService {
	override fun back() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.AUTHORISATION)
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
			ApiEndpoint(name = MoneysoftEndpoint.FINANCIAL.label,
			            action = {
				            loadFinancialEndpoints()
			            }),
			ApiEndpoint(name = MoneysoftEndpoint.TRANSACTIONS.label,
			            action = {
				            loadTransactionEndpoints()
			            }),
			ApiEndpoint(name = MoneysoftEndpoint.USER.label,
			            action = {
				            loadUserEndpoints()
			            })
		)
	}

	internal fun loadFinancialEndpoints() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.FINANCIAL)
	}

	internal fun loadTransactionEndpoints() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.TRANSACTION)
	}

	internal fun loadUserEndpoints() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.USER)
	}
}