package com.moneysoft.moneysoftdemoapp.ui.dialogs

import android.app.Activity
import android.view.View
import android.widget.*
import com.moneysoft.moneysoftdemoapp.R
import com.moneysoft.moneysoftdemoapp.components.OptionsAdapter

class OptionsDialog<T>(private val activity: Activity): PopupDialog(activity) {
	var multiSelect = false
	val selections: List<T>
		get() = _selections.toList()
	val options: List<Pair<String, T>>
		get() = _options.toList()
	private var adapter: OptionsAdapter<T>? = null
	private val _options = mutableListOf<Pair<String, T>>()
	private var _selections = mutableListOf<T>()

	fun addOption(label: String,
	              value: T) {
		_options.add(Pair(label, value))
	}

	fun show() {
		super.show(layoutResource = R.layout.options_dialog)

		val optionsList = layout.findViewById<ListView>(R.id.options_list)

		adapter = OptionsAdapter(context = activity,
		                         resource = R.layout.option_cell,
		                         multiSelect = multiSelect)
		adapter!!.addAll(_options)
		optionsList.adapter = adapter
		optionsList.setOnItemClickListener {
			_: AdapterView<*>, item: View, position: Int, _: Long ->
				val selectedOption = options[position]

				if (!multiSelect) {
					_selections.add(selectedOption.second)
					hide()
					success?.invoke()
				}
				else {
					val checkbox = item.findViewById<CheckBox>(R.id.selectChkBox)

					if (_selections.contains(selectedOption.second)) {
						_selections.remove(selectedOption.second)
						checkbox?.isChecked = false
					}
					else {
						_selections.add(selectedOption.second)
						checkbox?.isChecked = true
					}
				}
		}

		dialog?.setView(layout)
		dialog?.show()
	}
}