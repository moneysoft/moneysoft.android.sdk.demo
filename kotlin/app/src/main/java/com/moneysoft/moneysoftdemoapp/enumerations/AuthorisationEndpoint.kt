package com.moneysoft.moneysoftdemoapp.enumerations

enum class AuthorisationEndpoint(val label: String) {
	LOGIN("/token"),
	LOGOUT("logout")
}