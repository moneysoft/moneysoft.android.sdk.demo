package com.moneysoft.moneysoftdemoapp.enumerations

enum class MoneysoftEndpoint(val label: String) {
	FINANCIAL("Financial"),
	TRANSACTIONS("Transactions"),
	USER("User")
}