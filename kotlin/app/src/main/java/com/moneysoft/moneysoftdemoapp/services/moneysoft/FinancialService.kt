package com.moneysoft.moneysoftdemoapp.services.moneysoft

import android.text.InputType
import android.widget.EditText
import android.widget.TextView
import com.google.gson.Gson
import com.moneysoft.moneysoftdemoapp.enumerations.FinancialEndpoint
import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.Manager
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.moneysoftdemoapp.ui.dialogs.OptionsDialog
import com.moneysoft.sdk.api.MoneysoftApi
import com.moneysoft.sdk.common.models.financial.RefreshAccountOptions
import com.moneysoft.sdk.common.models.financial.UpdateTransactionOptions
import com.moneysoft.sdk.enumerations.ErrorCode
import com.moneysoft.sdk.enumerations.ErrorKey
import com.moneysoft.sdk.enumerations.financial.InstitutionCredentialPromptType
import com.moneysoft.sdk.listeners.ApiListListener
import com.moneysoft.sdk.listeners.ApiListener
import com.moneysoft.sdk.models.financial.*
import com.moneysoft.sdk.models.transactions.FinancialTransactionModel
import java.util.*

class FinancialService : IService {
	val PROVIDER_ID = 6
	val DEMO_BANK_PROVIDER_INSTITUTION_ID = 1201
	val DEMO_BANK_FINANCIAL_INSTITUTION_ID = 253
	val DEMO_BANK_FINANCIAL_SERVICE_ID = 733
	var DEMO_BANK_INSTITUTION_ID = ""
	var credentialsForm: InstitutionCredentialsFormModel? = null
	var selectedInstitution: FinancialInstitutionModel? = null
	var selectedAccounts: List<FinancialAccountLinkModel> = emptyList()

	fun getInstitutions() {
		val msApi = MoneysoftApi()
		val institutionsListener = ApiListListener<FinancialInstitutionModel>(successHandler = {
			institutions ->
				val optionsDialog = OptionsDialog<FinancialInstitutionModel>(Manager.Dialogs.activity)

				for(institution in institutions) {
					optionsDialog.addOption(label = institution.name ?: "",
					                        value = institution)
				}

				optionsDialog.onSuccess {
					selectedInstitution = optionsDialog.selections.first()
				}

				optionsDialog.show()
		}, errorHandler = {

		})

		msApi.financial().getInstitutions(listener = institutionsListener)
	}

	fun getPrompts() {
		val msApi = MoneysoftApi()
		val credentialsFormListener = ApiListener<InstitutionCredentialsFormModel>(successHandler = {
			credentialsForm = it
		}, errorHandler = {

		})

		if (selectedInstitution != null) {
			msApi.financial().getSignInForm(institution = selectedInstitution!!,
			                                listener = credentialsFormListener)
		}
		else {

		}
	}

	fun prepareGetLinkableAccounts() {
		if (credentialsForm != null) {
			val inputDialog = Manager.Dialogs.createInputDialog()

			inputDialog.header = "Sign In"

			for(prompt in credentialsForm!!.prompts) {
				val input = EditText(Manager.Dialogs.activity)

				if (prompt.type == InstitutionCredentialPromptType.PASSWORD) {
					input.inputType = InputType.TYPE_CLASS_TEXT.or(InputType.TYPE_TEXT_VARIATION_PASSWORD)
				}

				inputDialog.addInput(label = prompt.label,
				                     component = input,
				                     tag = prompt.index.toString())
			}

			inputDialog.onSuccess {
				val taggedInputs = inputDialog.inputs.filter {
					it.tag != null
				}

				for(input in taggedInputs) {
					val matchingPrompt = credentialsForm!!.prompts.find {
						it.index.toString().equals(input.tag)
					}

					matchingPrompt?.savedValue = (input as EditText).text.toString()
				}

				getLinkableAccounts()
			}

			inputDialog.show()
		}
	}

	fun getLinkableAccounts() {
		val msApi = MoneysoftApi()
		val linkAccountListener = ApiListListener<FinancialAccountLinkModel>(successHandler = {
			linkableAccounts ->
				val optionsDialog = OptionsDialog<FinancialAccountLinkModel>(Manager.Dialogs.activity)

				for(account in linkableAccounts) {
					optionsDialog.addOption(label = account.name ?: "",
					                        value = account)
				}

				optionsDialog.multiSelect = true
				optionsDialog.onSuccess {
					selectedAccounts = optionsDialog.selections
				}
				optionsDialog.show()
		}, errorHandler = {

		})

		msApi.financial().getLinkableAccounts(credentials = credentialsForm!!,
		                                      listener = linkAccountListener)
	}

	fun getLinkableAccountsMFA() {
		credentialsForm?.prompts!![0].savedValue = "user02"
		credentialsForm?.prompts!![1].savedValue = "user02"

		val msApi = MoneysoftApi()
		val linkAccountListener = ApiListListener<FinancialAccountLinkModel>(successHandler = {
			for(account in it) {
				println("RETRIEVED: ${account.name}")
			}
		}, errorHandler = {
			error ->
			if (error.code == ErrorCode.REQUIRES_MFA.value) {
				val rawFieldType = error.messages?.get(ErrorKey.MFA_FIELD_TYPE.value)?.toString()!!
				var fieldType = InstitutionCredentialPromptType.TEXT

				if (rawFieldType.equals("password", ignoreCase = true)) {
					fieldType = InstitutionCredentialPromptType.PASSWORD
				}

				val verification = MFAVerificationModel(institutionId = credentialsForm?.providerInstitutionId!!,
				                                        label = error.messages?.get(ErrorKey.MFA_PROMPT.value)?.toString()!!,
				                                        type = fieldType,
				                                        savedValue = "user02otp")
				msApi.financial().sendMFA(verification = verification)
			}
		})

		msApi.financial().getLinkableAccounts(credentials = credentialsForm!!,
		                                      listener = linkAccountListener)
	}

	fun linkAccounts() {
		val msApi = MoneysoftApi()
		val linkListener = ApiListListener<FinancialAccountModel>(successHandler = {
			linkedAccounts ->
				linkedAccounts.forEach {
					println("DemoApp::FinancialService::linkAccounts() -> ${it.name} | ${it.balance}")
				}
		}, errorHandler = {
			error ->
				println("DemoApp::FinancialService::linkAccounts()::error -> ${error.code} | ${error.messages}")
		})

		msApi.financial().linkAccounts(accounts = selectedAccounts,
									   includeTransactions = true,
		                               listener = linkListener)
	}

	fun prepareGetAccount() {
		val optionsDialog = OptionsDialog<Int>(Manager.Dialogs.activity)
		val msApi = MoneysoftApi()

		val accountsListener = ApiListListener<FinancialAccountModel>(successHandler = {
			accounts ->
				accounts.forEach {
					account ->
						optionsDialog.addOption(account.nickname ?: account.name, account.financialAccountId)
				}

				optionsDialog.onSuccess {
					getAccount(accountId = optionsDialog.selections.first())
				}

				optionsDialog.show()
		}, errorHandler = {

		})

		msApi.financial().getAccounts(listener = accountsListener)
	}

	fun prepareRefreshAccount() {
		val optionsDialog = OptionsDialog<Int>(Manager.Dialogs.activity)
		val msApi = MoneysoftApi()

		val accountsListener = ApiListListener<FinancialAccountModel>(successHandler = {
			accounts ->
			accounts.forEach {
				account ->
				optionsDialog.addOption(account.nickname ?: account.name, account.financialAccountId)
			}

			optionsDialog.onSuccess {
				refreshAccount(accountId = optionsDialog.selections.first())
			}

			optionsDialog.show()
		}, errorHandler = {

		})

		msApi.financial().getAccounts(listener = accountsListener)
	}

	fun prepareUpdateTransactions() {
		val optionsDialog = OptionsDialog<Int>(Manager.Dialogs.activity)
		val msApi = MoneysoftApi()

		val accountsListener = ApiListListener<FinancialAccountModel>(successHandler = {
			accounts ->
				accounts.forEach {
					account ->
						optionsDialog.addOption(account.nickname ?: account.name, account.financialAccountId)
				}

				optionsDialog.onSuccess {
					updateTransactions(accountId = optionsDialog.selections.first())
				}

				optionsDialog.show()
		}, errorHandler = {

		})

		msApi.financial().getAccounts(listener = accountsListener)
	}

	fun refreshAccounts() {
		val msApi = MoneysoftApi()

		val accountsListener = ApiListListener<FinancialAccountModel>(successHandler = {
			accounts ->
			val refreshListener = ApiListListener<FinancialAccountModel>(successHandler = {
				refreshedAccounts ->
                accounts.forEachIndexed { index, account ->
                    println("DemoApp::FinancialService::refreshAccounts() -> ${account.name} | ${refreshedAccounts[index]}")
                }
			}, errorHandler = {
				error ->
				println("DemoApp::FinancialService::refreshAccounts()::error -> ${error.code} | ${error.messages}")
			})

			val refreshOptions = RefreshAccountOptions()
			refreshOptions.includeTransactions = true

			msApi.financial().refreshAccounts(financialAccounts = accounts,
			                                 options = refreshOptions,
			                                 listener = refreshListener)
		}, errorHandler = {
			error ->
			println("DemoApp::FinancialService::refreshAccounts()::error -> ${error.code} | ${error.messages}")
		})

		msApi.financial().getAccounts(listener = accountsListener)
	}

	override fun back() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.MONEYSOFT)
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
			ApiEndpoint(name = FinancialEndpoint.INSTITUTIONS.label,
			            action = {
				            getInstitutions()
			            }),
			ApiEndpoint(name = FinancialEndpoint.PROMPTS.label,
			            action = {
				            getPrompts()
			            }),
			ApiEndpoint(name = FinancialEndpoint.LINKABLE_ACCOUNTS.label,
			            action = {
				            prepareGetLinkableAccounts()
			            }),
			ApiEndpoint(name = FinancialEndpoint.LINKABLE_ACCOUNTS_MFA.label,
			            action = {
				            getLinkableAccountsMFA()
			            }),
			ApiEndpoint(name = FinancialEndpoint.LINK_ACCOUNTS.label,
			            action = {
				            linkAccounts()
			            }),
			ApiEndpoint(name = FinancialEndpoint.GET_ACCOUNT.label,
			            action = {
				            prepareGetAccount()
			            }),
//			ApiEndpoint(name = FinancialEndpoint.GET_ACCOUNTS.label,
//			            action = {
//				            getAccounts()
//			            }),
			ApiEndpoint(name = FinancialEndpoint.REFRESH_ACCOUNT.label,
			            action = {
				            prepareRefreshAccount()
			            }),
			ApiEndpoint(name = FinancialEndpoint.REFRESH_ACCOUNTS.label,
			            action = {
				            refreshAccounts()
			            }),
			ApiEndpoint(name = FinancialEndpoint.UPDATE_TRANSACTIONS.label,
			            action = {
				            prepareUpdateTransactions()
			            })
//			ApiEndpoint(name = FinancialEndpoint.UNLINK_ACCOUNTS.label,
//			            action = {
//				            unlinkAccounts()
//			            })
		)
	}

	private fun getAccount(accountId: Int) {
		val msApi = MoneysoftApi()
		val accountListener = ApiListener<FinancialAccountModel>(successHandler = {
			account ->
				println("DemoApp::FinancialService::getAccount() -> ${Gson().toJson(account)}")
		}, errorHandler = {
			error ->
				println("DemoApp::FinancialService::getAccount()::error -> ${error.code} | ${error.messages}")
		})

		msApi.financial().getAccount(accountId = accountId,
		                             listener = accountListener)
	}

	private fun updateTransactions(accountId: Int) {
		val msApi = MoneysoftApi()

		val accountListener = ApiListener<FinancialAccountModel>(successHandler = {
			account ->
				val transactionsListener = ApiListListener<FinancialTransactionModel>(successHandler = {
					newTransactions ->
					newTransactions.forEach {
						print("Transaction: ${it.transactionId} | ${it.name} | ${it.amount} | ${it.date}")
					}
				}, errorHandler = {
					error ->
						println("DemoApp::FinancialService::updateTransactions()::error -> ${error.code} | ${error.messages}")
				})

				val startDate = Calendar.getInstance()
				startDate.add(Calendar.MONTH, -6)

				val updateOptions = UpdateTransactionOptions()
				updateOptions.startDate = startDate.time

				msApi.financial().updateTransactions(financialAccount = account,
				                                     options = updateOptions,
				                                     listener = transactionsListener)
		}, errorHandler = {
			error ->
				println("DemoApp::FinancialService::updateTransactions()::error -> ${error.code} | ${error.messages}")
		})

		msApi.financial().getAccount(accountId = accountId,
		                             listener = accountListener)
	}

	private fun refreshAccount(accountId: Int) {
		val msApi = MoneysoftApi()

		val accountListener = ApiListener<FinancialAccountModel>(successHandler = {
			account ->
			val refreshListener = ApiListener<FinancialAccountModel>(successHandler = {
				refreshedAccount ->
					println("DemoApp::FinancialService::refreshAccount() -> ${refreshedAccount.name} | ${refreshedAccount.balance}")
			}, errorHandler = {
				error ->
					println("DemoApp::FinancialService::refreshAccount()::error -> ${error.code} | ${error.messages}")
			})

//			msApi.financial().refreshAccount(financialAccount = account,
//			                                 includeTransactions = true,
//			                                 listener = refreshListener)
		}, errorHandler = {
			error ->
			println("DemoApp::FinancialService::refreshAccount()::error -> ${error.code} | ${error.messages}")
		})

		msApi.financial().getAccount(accountId = accountId,
		                             listener = accountListener)
	}
}