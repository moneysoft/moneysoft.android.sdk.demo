package com.moneysoft.moneysoftdemoapp.services.moneysoft

import com.moneysoft.moneysoftdemoapp.enumerations.ServiceType
import com.moneysoft.moneysoftdemoapp.enumerations.UserEndpoint
import com.moneysoft.moneysoftdemoapp.factories.ServiceFactory
import com.moneysoft.moneysoftdemoapp.managers.ServiceManager
import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint
import com.moneysoft.moneysoftdemoapp.services.IService
import com.moneysoft.sdk.api.MoneysoftApi
import com.moneysoft.sdk.listeners.ApiListener
import com.moneysoft.sdk.models.user.UserModel

class UserService: IService {
	fun getProfile() {
		val msApi = MoneysoftApi()
		val profileListener = ApiListener<UserModel>(successHandler = {
			profile ->
				println("DemoApp::UserService::getProfile() -> ${profile.userId} | ${profile.email}")
		}, errorHandler = {
			error ->
				println("DemoApp::UserService::getProfile()::error -> ${error.code} | ${error.messages}")
		})

		msApi.user().getProfile(listener = profileListener)
	}
	override fun back() {
		ServiceManager.activeService = ServiceFactory().create(ServiceType.MONEYSOFT)
	}

	override fun getEndpoints(): List<ApiEndpoint> {
		return listOf(
			ApiEndpoint(name = UserEndpoint.PROFILE.label,
			            action = {
				            getProfile()
			            })
		)
	}
}