package com.moneysoft.moneysoftdemoapp.enumerations

enum class TransactionEndpoint(val label: String) {
	GET_TRANSACTIONS("Get Transactions"),
	GET_CATEGORIES("Get Categories")
}