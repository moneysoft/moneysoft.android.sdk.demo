package com.moneysoft.moneysoftdemoapp.services

import com.moneysoft.moneysoftdemoapp.models.ApiEndpoint

interface IService {
	fun back()
	fun getEndpoints(): List<ApiEndpoint>
}