package com.example.nathanphillips.mobilesdkdemoapp;

import android.content.Context;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.activities.AuthorisationActivity;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4ClassRunner.class)
public class ExampleInstrumentedTest {


    @Rule
    public ActivityTestRule<AuthorisationActivity> activityRule = new ActivityTestRule<>(AuthorisationActivity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getContext();

        assertEquals("com.moneysoft.android.sdk.demo", appContext.getPackageName());
    }

//    @Before
//    public void init() {
//        IdlingRegistry.getInstance()
//                .register(//Singletons.getInstance().getNetworkCallQueue());
//    }

    @Test
    public void GotoGoalActivity() {
        onData(instanceOf(ApiEndpoint.class)).inAdapterView(withId(R.id.endpoint_list)).atPosition(0).perform(click());
        onData(instanceOf(ApiEndpoint.class)).inAdapterView(withId(R.id.endpoint_list)).atPosition(2).perform(click());
        onData(instanceOf(ApiEndpoint.class)).inAdapterView(withId(R.id.endpoint_list)).atPosition(1).perform(click());
    }
}
