package com.moneysoft.mobilesdkdemoapp.activities;

import android.os.Bundle;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.mobilesdkdemoapp.services.UserService;

public class UserActivity extends EndpointActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user);

		setupEndpoints();
		setupList();
	}

	@Override
	protected void setupEndpoints() {
		endpoints.add(new ApiEndpoint("/api/2.0/user", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/adviser", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/all", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/state", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/engagement", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/confirmAccounts", "PUT"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/permissions", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/creditCard", "GET"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/creditCard", "POST"));
		endpoints.add(new ApiEndpoint("/api/2.0/user/creditCard", "DELETE"));
	}

	@Override
	protected void handleClick(ApiEndpoint endpoint) {
		//Singletons.getInstance().getNetworkCallQueue().increment();
		UserService userService = new UserService();

		userService.callEndpoint(endpoint);
	}
}
