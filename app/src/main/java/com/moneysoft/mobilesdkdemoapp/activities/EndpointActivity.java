package com.moneysoft.mobilesdkdemoapp.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.mobilesdkdemoapp.models.EndpointField;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class EndpointActivity extends AppCompatActivity {
	protected List<ApiEndpoint> endpoints = new ArrayList<>();
	private final int FIELD_MARGIN_LEFT = 35;
	private final int FIELD_MARGIN_RIGHT = 35;
	private final int COMPONENT_HEIGHT = 120;

	protected abstract void setupEndpoints();

	protected void setupList() {
		ListView endpointList = findViewById(R.id.endpoint_list);
		endpointList.setAdapter(new ArrayAdapter<ApiEndpoint>(this, R.layout.endpoint_list_item, endpoints) {
			@NotNull
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View row = convertView;

				if (row == null) {
					LayoutInflater inflater = EndpointActivity.this.getLayoutInflater();
					row = inflater.inflate(R.layout.endpoint_list_item, parent, false);
				}

				TextView endpointLbl = row.findViewById(R.id.endpoint_lbl);

				if (endpointLbl != null) {
					endpointLbl.setText(endpoints.get(position).getName());
				}

				TextView methodLbl = row.findViewById(R.id.method_lbl);

				if (methodLbl != null) {
					methodLbl.setText(endpoints.get(position).getMethod());
				}

				return row;
			}
		});

		endpointList.setItemsCanFocus(true);

		endpointList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
				handleClick(endpoints.get(position));
			}
		});
	}

	private Button drawExpandedFields(View view, EndpointField[] fields) {
		Button submitBtn = null;

		if (fields != null && fields.length > 0) {
			RelativeLayout expandableContainer = view.findViewById(R.id.expandable_content);

			if (expandableContainer != null) {
				expandableContainer.removeAllViews();

				int topMargin = 0;

				for (EndpointField field : fields) {
					switch (field.getType()) {
						case SUBMIT:
							submitBtn = new Button(EndpointActivity.this);
							submitBtn.setText(field.getName());

							RelativeLayout.LayoutParams buttonParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							buttonParams.setMargins(0, topMargin, 0, 0);
							buttonParams.setMarginStart(FIELD_MARGIN_LEFT);
							buttonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

							expandableContainer.addView(submitBtn, buttonParams);

							break;
						case TEXT:
						default:
							EditText inputFld = new EditText(EndpointActivity.this);
							inputFld.setHint(field.getName());
							inputFld.setTag(field.getName());

							RelativeLayout.LayoutParams inputParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							inputParams.setMargins(0, topMargin, 0, 0);
							inputParams.setMarginStart(FIELD_MARGIN_LEFT);
							inputParams.setMarginEnd(FIELD_MARGIN_RIGHT);
							expandableContainer.addView(inputFld, inputParams);

							break;
					}

					topMargin += COMPONENT_HEIGHT;
				}
			}
		}

		return submitBtn;
	}

	private void hideExpandedFields(View view) {
		RelativeLayout expandableContainer = view.findViewById(R.id.expandable_content);

		if (expandableContainer != null) {
			expandableContainer.removeAllViews();
		}
	}

	private ProgressDialog progressDialog;


	public void showProgress() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				progressDialog = ProgressDialog.show(EndpointActivity.this, "", "Please wait", true, false);
			}
		});
	}

	public void hideProgress() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (progressDialog != null) {
					progressDialog.cancel();
					progressDialog.dismiss();
				}
			}
		});
	}

	public void showToast(String msg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(EndpointActivity.this, msg, Toast.LENGTH_SHORT).show();

			}
		});
	}

	protected abstract void handleClick(ApiEndpoint endpoint);
}
