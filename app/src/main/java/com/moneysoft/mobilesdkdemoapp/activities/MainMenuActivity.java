package com.moneysoft.mobilesdkdemoapp.activities;

import android.content.Intent;
import android.os.Bundle;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;

public class MainMenuActivity extends EndpointActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        setupEndpoints();
        setupList();
//        new Handler().postDelayed(() -> startActivity(GoalsActivity.class), 1000);
    }

    @Override
    protected void setupEndpoints() {

        endpoints.add(new ApiEndpoint("User", ""));
        endpoints.add(new ApiEndpoint("Financial", ""));
        endpoints.add(new ApiEndpoint("Goals", ""));
    }

    @Override
    protected void handleClick(ApiEndpoint endpoint) {
        switch (endpoint.getName().toLowerCase()) {
            case "user":
                startActivity(UserActivity.class);
                break;
            case "financial":
                startActivity(FinancialActivity.class);
                break;
            case "goals":
                startActivity(GoalsActivity.class);
                break;
        }
    }

    private void startActivity(Class activity) {
        startActivity(new Intent(MainMenuActivity.this, activity));
    }
}
