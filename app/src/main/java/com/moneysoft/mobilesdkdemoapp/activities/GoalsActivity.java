package com.moneysoft.mobilesdkdemoapp.activities;

import android.os.Bundle;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.mobilesdkdemoapp.services.GoalsService;

public class GoalsActivity extends EndpointActivity {
    private GoalsService goalsService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);
        goalsService = new GoalsService(this);
        setupEndpoints();
        setupList();
    }

    @Override
    protected void setupEndpoints() {
        endpoints.add(new ApiEndpoint("/api/2.0/Goals", "POST"));
        endpoints.add(new ApiEndpoint("/api/2.0/Goals", "PUT"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals", "GET"));
        endpoints.add(new ApiEndpoint("/api/2.0/Goals/GoalId", "GET"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/GoalId", "DELETE"));
        endpoints.add(new ApiEndpoint("/api/2.0/Goals/GoalSummary", "GET"));

        endpoints.add(new ApiEndpoint("/api/2.0/Goals/Contribute", "POST"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/Contributions", "GET"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/Contributions", "POST"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/Contributions/GoalId", "GET"));

        endpoints.add(new ApiEndpoint("/api/2.0/Goals/Transaction", "POST"));
        endpoints.add(new ApiEndpoint("/api/2.0/Goals/Transaction/TransactionId", "DELETE"));

        endpoints.add(new ApiEndpoint("/api/1.1/Goals/GoalAccounts", "POST"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/GoalAccounts", "DELETE"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/GoalAccounts/GoalId", "GET"));
        endpoints.add(new ApiEndpoint("/api/1.1/Goals/GoalAccounts/GoalId", "DELETE"));

        endpoints.add(new ApiEndpoint("/api/2.0/Goals/Image", "POST"));
        endpoints.add(new ApiEndpoint("/api/2.0/Goals/DefaultImage", "GET"));

        endpoints.add(new ApiEndpoint("Statistics/Goal", "GET"));

    }

    @Override
    protected void handleClick(ApiEndpoint endpoint) {
        goalsService.callEndpoint(endpoint);

    }
}