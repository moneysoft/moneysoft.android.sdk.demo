package com.moneysoft.mobilesdkdemoapp.activities;

import android.os.Bundle;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.enumerations.FieldType;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.mobilesdkdemoapp.models.EndpointField;
import com.moneysoft.mobilesdkdemoapp.services.AuthorisationService;

public class AuthorisationActivity extends EndpointActivity {
    AuthorisationService authorisationService = new AuthorisationService(this);
    ApiEndpoint tokenEndpoint = new ApiEndpoint("/token", "POST", true, new EndpointField[]{
            new EndpointField("Username", FieldType.TEXT),
            new EndpointField("Password", FieldType.TEXT),
            new EndpointField("Submit", FieldType.SUBMIT)
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        setupEndpoints();
        setupList();
//		new Handler().postDelayed(() -> handleClick(tokenEndpoint),1000);
    }

    @Override
    protected void setupEndpoints() {
        endpoints.add(tokenEndpoint);

        endpoints.add(new ApiEndpoint("/logout", "POST", false));
    }

    @Override
    protected void handleClick(ApiEndpoint endpoint) {
        //Singletons.getInstance().networkCallQueue.increment();
        authorisationService.callEndpoint(endpoint);
    }
}
