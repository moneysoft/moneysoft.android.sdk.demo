package com.moneysoft.mobilesdkdemoapp.activities;

import android.os.Bundle;
import com.moneysoft.mobilesdkdemoapp.R;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.mobilesdkdemoapp.services.FinancialService;

public class FinancialActivity extends EndpointActivity {
    public static final String[] endpointList = {
            "1. FinancialInstitution",
            "2. getSignInForms",
            "3. loginIntoBankAccount",
            "4. getListofLinkableAccounts",
            "5. linkAccount",
            "6. showAccountDetails",
            "7. showAccountsList",
            "8. unlinkAccount",
            "9. refreshAccount",
            "10. transactions"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_financial);

        setupEndpoints();
        setupList();
    }

    @Override
    protected void setupEndpoints() {
        for(String p : endpointList){
            endpoints.add(new ApiEndpoint(p, "GET"));
        }
    }
    @Override
    protected void handleClick(ApiEndpoint endpoint) {
        //Singletons.getInstance().getNetworkCallQueue().increment();
        FinancialService userService = new FinancialService(this);

        userService.callEndpoint(endpoint);
    }
}
