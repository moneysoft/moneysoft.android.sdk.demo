package com.moneysoft.mobilesdkdemoapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import com.moneysoft.mobilesdkdemoapp.activities.AuthorisationActivity;
import com.moneysoft.mobilesdkdemoapp.activities.BaseActivity;
import com.moneysoft.sdk.api.MoneysoftApi;
import org.jetbrains.annotations.NotNull;

import javax.net.ssl.*;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

public class MainActivity extends BaseActivity {
	private static final String REFERRER_URL = "https://localhost";
	private static final String ENDPOINT_BASE_URL = "https://api.beta.moneysoft.com.au";
	protected Button signInBtn = null;
	protected Button signOutBtn = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MoneysoftApi.configure(this, ENDPOINT_BASE_URL, REFERRER_URL, (WebView) findViewById(R.id.webViewMain), true, true);
		setupTrust();

		Intent authorisationIntent = new Intent(MainActivity.this, AuthorisationActivity.class);
		startActivity(authorisationIntent);
	}

	@SuppressLint({"TrustAllX509TrustManager","BadHostnameVerifier"})
	protected void setupTrust() {
		if (BuildConfig.DEBUG) {
			// This is not 100% safe, but for local development purposes it will suffice since I had issues with the app accepting a self-signed certificate
			try {
				TrustManager[] trustAllCerts = new TrustManager[] {
						new X509TrustManager() {
							public X509Certificate[] getAcceptedIssuers() {
								return new X509Certificate[0];
							}

							@Override
							public void checkClientTrusted(X509Certificate[] certs, String authType) { }

							@Override
							public void checkServerTrusted(X509Certificate[] certs, String authType) { }
						}
				};

				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
					@Override
					public boolean verify(String arg0, SSLSession arg1) {
						return true;
					}
				});
			}
			catch (Exception e) {

			}
		}
	}
}
