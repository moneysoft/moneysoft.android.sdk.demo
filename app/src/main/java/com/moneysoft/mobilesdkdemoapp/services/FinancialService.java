package com.moneysoft.mobilesdkdemoapp.services;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.moneysoft.mobilesdkdemoapp.activities.FinancialActivity;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.sdk.api.MoneysoftApi;
import com.moneysoft.sdk.listeners.interfaces.IApiListListener;
import com.moneysoft.sdk.listeners.interfaces.IApiListener;
import com.moneysoft.sdk.models.errors.ApiErrorModel;
import com.moneysoft.sdk.models.financial.FinancialAccountLinkModel;
import com.moneysoft.sdk.models.financial.FinancialAccountModel;
import com.moneysoft.sdk.models.financial.FinancialInstitutionModel;
import com.moneysoft.sdk.models.financial.InstitutionCredentialsFormModel;
import com.moneysoft.sdk.models.transactions.FinancialTransactionModel;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class FinancialService implements IService {
    private static ArrayList<FinancialInstitutionModel> financialInstitutions;
    private static InstitutionCredentialsFormModel      institutionCredentialsForm;
    private static ArrayList<FinancialAccountLinkModel> linkableAccounts;
    private static FinancialAccountModel                linkedAccount;
    private static ArrayList<FinancialAccountModel>     linkedAccountList;
    private static FinancialAccountModel                selectedLinkedAccount;
    private static ArrayList<FinancialTransactionModel> transactions;
    final          MoneysoftApi                         msApi = new MoneysoftApi();
    FinancialActivity activity;

    public FinancialService(FinancialActivity ctx) {
        this.activity = ctx;
    }

    @Override
    public void callEndpoint(ApiEndpoint endpoint) {
        int index = -1;
        for (int i = 0; i < FinancialActivity.endpointList.length; i++) {
            if(Objects.equals(endpoint.getName(), FinancialActivity.endpointList[i])){
                index = i;
                break;
            }
        }
        switch (index) {
            case 0:
                getInstitutions();
                break;
            case 1:
                getSignInForms();
                break;
            case 2:
                loginIntoBankAccount();
                break;
            case 3:
                getListOfLinkableAccounts();
                break;
            case 4:
                linkAccount();
                break;
            case 5:
                if (linkedAccount == null) {
                    showAccountsList();
                }
                else {
                    showAccountDetails();
                }
                break;
            case 6:
                showAccountsList();
                break;
            case 7:
                FinancialAccountModel accountU = linkedAccount != null ? linkedAccount : selectedLinkedAccount;
                unlinkAccount(accountU);
                break;
            case 8:
                FinancialAccountModel accountR = linkedAccount != null ? linkedAccount : selectedLinkedAccount;
                refreshAccount(accountR);
                break;
            case 9:
                showTransactions();
                break;

        }
    }

    private void getInstitutions() {
        activity.showProgress();
        msApi.financial().getInstitutions(new IApiListListener<FinancialInstitutionModel>() {
            @Override
            public void onSuccess(@NotNull final List<? extends FinancialInstitutionModel> list) {
                financialInstitutions = Lists.newArrayList(list);
                activity.hideProgress();
                activity.showToast("Institution List ready.");
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                Log.e("getInstitutions", "onError: " + apiErrorModel);
                activity.hideProgress();
            }
        });
    }


    private void getSignInForms() {
        List<CharSequence> dialogItems = new ArrayList<>(financialInstitutions.size());
        for (FinancialInstitutionModel item : financialInstitutions) {
            dialogItems.add(item.getName());
        }
        new AlertDialog.Builder(activity).setItems(dialogItems.toArray(new CharSequence[0]),
                                                   new DialogInterface.OnClickListener() {
                                                       @Override
                                                       public void onClick(DialogInterface dialogInterface, int i) {
                                                           dialogInterface.dismiss();
                                                           getSignInForms(msApi, financialInstitutions.get(i));
                                                       }
                                                   }).create().show();
    }

    private void getSignInForms(final MoneysoftApi msApi, FinancialInstitutionModel fim) {
        activity.showProgress();
        msApi.financial().getSignInForm(fim, new IApiListener<InstitutionCredentialsFormModel>() {
            @Override
            public void onSuccess(final InstitutionCredentialsFormModel icfm) {
                institutionCredentialsForm = icfm;
                activity.hideProgress();
                activity.showToast("Forms ready");
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                Log.e("getSignInForms", "onError: " + new Gson().toJson(apiErrorModel));
                activity.hideProgress();
            }
        });
    }

    private void loginIntoBankAccount() {
        ArrayList<String>
                dialogItems =
                Lists.newArrayList("financialInstitutionId = " + institutionCredentialsForm.getFinancialInstitutionId(),
                                   "financialServiceId = " + institutionCredentialsForm.getFinancialServiceId());
        for (int i = 0; i < institutionCredentialsForm.getPrompts().size(); i++) {
            dialogItems.add("Prompt " + (i + 1) + " = " + institutionCredentialsForm.getPrompts().get(i).getLabel());
        }

        new AlertDialog
                .Builder(activity)
                .setItems(dialogItems.toArray(new CharSequence[0]), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        loginIntoBankAccount(institutionCredentialsForm);
                    }
                })
                .setNegativeButton("Cancel", (new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }))
                .create()
                .show();
    }

    private void loginIntoBankAccount(final InstitutionCredentialsFormModel icfm) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout ll = new LinearLayout(activity);
                ll.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT));
                ll.setOrientation(LinearLayout.VERTICAL);
                final EditText[] inputs = new EditText[icfm.getPrompts().size()];
                for (int i = 0; i < icfm.getPrompts().size(); i++) {
                    inputs[i] = new EditText(activity);
                    inputs[i].setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    inputs[i].setHint(icfm.getPrompts().get(i).getLabel());
                    ll.addView(inputs[i]);
                }

                new AlertDialog.Builder(activity)
                        .setTitle("Title")
                        .setView(ll)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                for (int i = 0; i < inputs.length; i++) {
                                    icfm.getPrompts().get(i).setSavedValue(inputs[i].getText().toString());
                                }
                                activity.showToast("Credentials saved...");
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }
        });
    }

    private void getListOfLinkableAccounts() {
        FinancialService.this.getListOfLinkableAccounts(msApi, institutionCredentialsForm);
    }

    private void getListOfLinkableAccounts(MoneysoftApi msApi, InstitutionCredentialsFormModel icfm) {
        msApi.financial().getLinkableAccounts(icfm, new IApiListListener<FinancialAccountLinkModel>() {
            @Override
            public void onSuccess(@NotNull List<? extends FinancialAccountLinkModel> list) {
                linkableAccounts = Lists.newArrayList(list);
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                //Singletons.getInstance().getNetworkCallQueue().decrement();
                Log.e("getLinkableAccounts", "onError: " + apiErrorModel);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "" + apiErrorModel.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void linkAccount() {
        String[] alertItems = new String[linkableAccounts.size()];
        for (int i = 0; i < linkableAccounts.size(); i++) {
            alertItems[i] = String.format("#%s, N:%s, $%s",
                                          linkableAccounts.get(i).getNumber(),
                                          linkableAccounts.get(i).getName(),
                                          linkableAccounts.get(i).getBalance());
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity).setItems(alertItems, (new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        linkAccount(linkableAccounts.get(i));
                    }
                })).create().show();
            }
        });
    }

    private void linkAccount(FinancialAccountLinkModel alm) {
        msApi.financial().linkAccount(alm, new IApiListener<FinancialAccountModel>() {
            @Override
            public void onSuccess(FinancialAccountModel accountModel) {
                linkedAccount = accountModel;
                showAccountDetails(accountModel);
            }

            @Override
            public void onError(@NotNull ApiErrorModel error) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void showAccountDetails() {
        showAccountDetails(linkedAccount);
    }

    private void showAccountDetails(FinancialAccountModel accountModel) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity)
                        .setMessage(String.format(Locale.getDefault(), "finAccId = %d,\n" +
                                                                       "prvFinAccId = %s,\n" +
                                                                       "finInsts = %s,\n" +
                                                                       "prvId = %d,\n" +
                                                                       "name = %s,\n" +
                                                                       "nickName = %s,\n" +
                                                                       "num = %s,\n" +
                                                                       "balance = %.2f,\n" +
                                                                       "type = %s,\n" +
                                                                       "created = %s,\n" +
                                                                       "prvLastUpd = %s,\n" +
                                                                       "disabled = %s,\n" +
                                                                       "prvContId = %s,\n" +
                                                                       "blncOnly = %s,\n" +
                                                                       "manual = %s,\n" +
                                                                       "incBdgt = %s,\n" +
                                                                       "assetCat = %s",
                                                  accountModel.getFinancialAccountId(),
                                                  accountModel.getProviderAccountId(),
                                                  String.valueOf(accountModel.getFinancialInstitution()),
                                                  accountModel.getProviderInstitutionId(),
                                                  accountModel.getName(),
                                                  accountModel.getNickname(),
                                                  accountModel.getNumber(),
                                                  accountModel.getBalance(),
                                                  accountModel.getType().toString(),
                                                  accountModel.getCreated().toString(),
                                                  String.valueOf(accountModel.getLastSuccessfulUpdateUtc()),
                                                  accountModel.getDisabled(),
                                                  accountModel.getProviderContainerId(),
                                                  accountModel.isBalanceOnly(),
                                                  accountModel.isManualAccount(),
                                                  accountModel.isInBudget(),
                                                  accountModel.getAssetCategory().toString()))
                        .setPositiveButton("Update", (dialog, which) -> refreshAccount(accountModel))
                        .setNegativeButton("Unlink", (dialog, which) -> unlinkAccount(accountModel))
                        .setNeutralButton("Transactions",
                                          (dialog, which) -> msApi.financial()
                                                  .updateTransactions(accountModel,
                                                                      new IApiListListener<FinancialTransactionModel>() {

                                                                          @Override
                                                                          public void onSuccess(@NotNull
                                                                                                        List<? extends FinancialTransactionModel> list) {
                                                                              transactions = Lists.newArrayList(list);
                                                                          }

                                                                          @Override
                                                                          public void onError(
                                                                                  @NotNull ApiErrorModel error) {
                                                                              activity.runOnUiThread(() -> Toast.makeText(
                                                                                      activity,
                                                                                      "" + error.toString(),
                                                                                      Toast.LENGTH_LONG).show());
                                                                          }

                                                                      }))
                        .show();
            }
        });
    }

    private void showAccountsList() {
        msApi.financial().getAccounts(new IApiListListener<FinancialAccountModel>() {
            @Override
            public void onSuccess(@NotNull List<? extends FinancialAccountModel> result) {
                linkedAccountList = Lists.newArrayList(result);
                String[] items = new String[result.size()];
                for (int i = 0; i < result.size(); i++) {
                    FinancialAccountModel fam = result.get(i);
                    items[i] =
                            String.format(Locale.getDefault(),
                                          "ID:%d, Name:%s, Num:%s, $%.2f",
                                          fam.getFinancialAccountId(),
                                          fam.getName(),
                                          fam.getNumber(),
                                          fam.getBalance());
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(activity)
                                .setItems(items, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        selectedLinkedAccount = linkedAccountList.get(which);
                                        showAccountDetails(selectedLinkedAccount);
                                    }
                                })
                                .show();
                    }
                });

            }

            @Override
            public void onError(@NotNull ApiErrorModel error) {

            }
        });
    }

    private void unlinkAccount(FinancialAccountModel accountModel) {
        if(accountModel == null){
            activity.showToast("Account not selected.");
            return;
        }
        msApi.financial().unlinkAccount(accountModel, new IApiListener<FinancialAccountModel>() {
            @Override
            public void onSuccess(FinancialAccountModel result) {
                Toast.makeText(activity, "Unlink successful", Toast.LENGTH_SHORT).show();
                showAccountsList();
            }

            @Override
            public void onError(@NotNull ApiErrorModel error) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void refreshAccount(FinancialAccountModel accountModel) {
        msApi.financial().refreshAccount(accountModel, false, new IApiListener<FinancialAccountModel>() {
            @Override
            public void onSuccess(FinancialAccountModel result) {
                Toast.makeText(activity, "Refresh successful", Toast.LENGTH_SHORT).show();
                showAccountsList();
            }

            @Override
            public void onError(@NotNull ApiErrorModel error) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, "" + error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void showTransactions() {
        String[] alertItems = new String[transactions.size()];
        for (int i = 0; i < transactions.size(); i++) {
            alertItems[i] = String.format("$%f, :%s, %s",
                                          transactions.get(i).getAmount(),
                                          transactions.get(i).getDate(),
                                          transactions.get(i).getType().toString());
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity).setItems(alertItems, (new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })).create().show();
            }
        });
    }
}
