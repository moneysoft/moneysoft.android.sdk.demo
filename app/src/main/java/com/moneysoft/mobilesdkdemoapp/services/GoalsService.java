package com.moneysoft.mobilesdkdemoapp.services;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.moneysoft.mobilesdkdemoapp.activities.GoalsActivity;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.sdk.api.MoneysoftApi;
import com.moneysoft.sdk.enumerations.goals.GoalType;
import com.moneysoft.sdk.listeners.interfaces.IApiListListener;
import com.moneysoft.sdk.listeners.interfaces.IApiListener;
import com.moneysoft.sdk.models.ApiResponseModel;
import com.moneysoft.sdk.models.errors.ApiErrorModel;
import com.moneysoft.sdk.models.goals.GoalContributionModel;
import com.moneysoft.sdk.models.goals.GoalModel;
import com.moneysoft.sdk.models.goals.GoalStatisticsModel;
import com.moneysoft.sdk.models.goals.GoalSummaryModel;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.*;

public class GoalsService implements IService {

    GoalsActivity activity;
    MoneysoftApi msApi;
    List<GoalModel> goals = Arrays.asList();

    public GoalsService(GoalsActivity ctx) {
        this.activity = ctx;
        msApi = new MoneysoftApi();
    }

    @Override
    public void callEndpoint(ApiEndpoint endpoint) {
        switch (endpoint.getName()) {
            case "/api/2.0/Goals":
                if (endpoint.getMethod().equalsIgnoreCase("POST"))
                    saveGoal();
                if (endpoint.getMethod().equalsIgnoreCase("PUT"))
                    updateGoal();
                break;
            case "/api/1.1/Goals":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getAll(true, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            GoalModel gm = goals.get(which);
                            showGoalDetailsDialog(gm);
                        }
                    });
                break;
            case "/api/2.0/Goals/GoalId":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getGoal();
                break;
            case "/api/1.1/Goals/GoalId":
                if (endpoint.getMethod().equalsIgnoreCase("DELETE"))
                    deleteGoal();
                break;
            case "/api/2.0/Goals/GoalSummary":
                if (endpoint.getMethod().equalsIgnoreCase("GET")) {
                    getGoalSummary();
                }
                break;

            case "/api/2.0/Goals/Contribute":
                if (endpoint.getMethod().equalsIgnoreCase("POST"))
                    saveManualContribution();
                break;
            case "/api/1.1/Goals/Contributions":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getAllContributions();
                if (endpoint.getMethod().equalsIgnoreCase("POST"))
                    saveContribution();
                break;
            case "/api/1.1/Goals/Contributions/GoalId":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getGoalContributions();
                break;

            case "/api/2.0/Goals/Transaction":
                if (endpoint.getMethod().equalsIgnoreCase("POST")) {
                    linkTransaction();
                }
                break;
            case "/api/2.0/Goals/Transaction/TransactionId":
                if (endpoint.getMethod().equalsIgnoreCase("DELETE"))
                    unlinkTransaction();
                break;

            case "/api/1.1/Goals/GoalAccounts":
                if (endpoint.getMethod().equalsIgnoreCase("POST"))
                    saveGoalAccount();

                if (endpoint.getMethod().equalsIgnoreCase("DELETE"))
                    deleteGoalAccount();
                break;
            case "/api/1.1/Goals/GoalAccounts/GoalId":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getGoalAccounts();

                if (endpoint.getMethod().equalsIgnoreCase("DELETE"))
                    deleteAllGoalAccounts();
                break;

            case "/api/2.0/Goals/Image":
                if (endpoint.getMethod().equalsIgnoreCase("POST")) {
                    saveGoalImage();
                }
                break;
            case "/api/2.0/Goals/DefaultImage":
                if (endpoint.getMethod().equalsIgnoreCase("GET")) {
                    getDefaultGoalImage();
                }
                break;

            case "Statistics/Goal":
                if (endpoint.getMethod().equalsIgnoreCase("GET"))
                    getGoalStatistics();
                break;
        }
    }

    private void getGoalStatistics() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                msApi.goals().getGoalStatistics(goals.get(which), new IApiListener<GoalStatisticsModel>() {
                    @Override
                    public void onSuccess(GoalStatisticsModel gsm) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();
                        activity.runOnUiThread(() -> {
                            new AlertDialog
                                    .Builder(activity)
                                    .setTitle("Goal Summary")
                                    .setMessage(String.format(Locale.getDefault(), "GoalId= %d\n" +
                                                    "description= %s\n" +
                                                    "totalMonths= %d\n" +
                                                    "remainingMonths= %d\n" +
                                                    "current= %d\n" +
                                                    "totalWithCurrencyTransform= %s\n" +
                                                    "startBalanceWithCurrencyTransform= %s\n" +
                                                    "totalContributionsIncStartBalance= %s\n" +
                                                    "remainingAmount= %.2f\n" +
                                                    "progress= %.2f\n" +
                                                    "monthly= %.2f\n" +
                                                    "remainingMonthly= %.2f\n" +
                                                    "currentPosition= %.2f\n" +
                                                    "isOnTrack= %s\n" +
                                                    "startMonthYearFormatted= %s\n" +
                                                    "endMonthYearFormatted= %s\n",
                                            gsm.getGoalID(),
                                            gsm.getDescription(),
                                            gsm.getTotalMonths(),
                                            gsm.getRemainingMonths(),
                                            gsm.getCurrent(),
                                            gsm.getTotalWithCurrencyTransform(),
                                            gsm.getStartBalanceWithCurrencyTransform(),
                                            gsm.getTotalContributionsIncStartBalance(),
                                            gsm.getRemainingAmount(),
                                            gsm.getProgress(),
                                            gsm.getMonthly(),
                                            gsm.getRemainingMonthly(),
                                            gsm.getCurrentPosition(),
                                            String.valueOf(gsm.isOnTrack()),
                                            gsm.getStartMonthYearFormatted(),
                                            gsm.getEndMonthYearFormatted()
                                    ))
                                    .show();
                        });

                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();
                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void deleteGoalAccount() {
        int testFinancialAccountId = 158800;
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                msApi.goals().saveGoalAccount(goals.get(which).getGoalId(), testFinancialAccountId, new IApiListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "Account was successfully unlinked from the goal", Toast.LENGTH_SHORT).show());
//                    }
//
//                    @Override
//                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error -> " + apiErrorModel.getMessages(), Toast.LENGTH_SHORT).show());
//                    }
//                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void saveGoalAccount() {
        int testFinancialAccountId = 158800;
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                msApi.goals().saveGoalAccount(goals.get(which).getGoalId(), testFinancialAccountId, new IApiListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//                        activity.runOnUiThread(() -> {
//                            Toast.makeText(activity, "Account was successfully linked to goal", Toast.LENGTH_SHORT).show();
//                        });
//
//                    }
//
//                    @Override
//                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error -> " + apiErrorModel.getMessages(), Toast.LENGTH_SHORT).show());
//                    }
//                });

            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void getGoalContributions() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                msApi.goals().getGoalContributions(goals.get(which).getGoalId(), new IApiListListener<GoalContributionModel>() {
                    @Override
                    public void onSuccess(@NotNull List<? extends GoalContributionModel> list) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> {

                            String[] dialogItems = new String[list.size()];
                            for (int i = 0; i < list.size(); i++) {
                                GoalContributionModel cm = list.get(i);
                                dialogItems[i] = String.format(
                                        Locale.getDefault(),
                                        "CID=%d, GID=%d, $%.2f, %%%.1f T=%d",
                                        cm.getId(),
                                        cm.getGoalId(),
                                        cm.getContributionAmount(),
                                        cm.getPercent(),
                                        cm.getContributionTypeId().getValue());
                            }
                            new AlertDialog
                                    .Builder(activity)
                                    .setTitle("GoalContributions")
                                    .setItems(dialogItems, (dialog1, which1) -> {
                                    })
                                    .create()
                                    .show();
                        });
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void deleteAllGoalAccounts() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                msApi.goals().deleteAllGoalAccounts(goals.get(which).getGoalId(), new IApiListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "All accounts were successfully unlinked from this goal", Toast.LENGTH_SHORT).show());
//                    }
//
//                    @Override
//                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error -> " + apiErrorModel.getMessages(), Toast.LENGTH_SHORT).show());
//                    }
//                });

            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void getGoalAccounts() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                msApi.goals().getGoalAccounts(goals.get(which).getGoalId(), new IApiListListener<Integer>() {
//                    @Override
//                    public void onSuccess(@NotNull List<? extends Integer> list) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//
//                        activity.runOnUiThread(() -> {
//                            String[] dialogItems = new String[list.size()];
//                            for (int i = 0; i < list.size(); i++)
//                                dialogItems[i] = String.valueOf(list.get(i));
//
//                            new AlertDialog
//                                    .Builder(activity)
//                                    .setTitle("Goal Accounts")
//                                    .setItems(dialogItems, (dialog1, which1) -> {
//                                    })
//                                    .create()
//                                    .show();
//                        });
//                    }
//
//                    @Override
//                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
//                        //Singletons.getInstance().getNetworkCallQueue().decrement();
//
//                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getMessages(), Toast.LENGTH_LONG).show());
//                    }
//                });

            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void saveContribution() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                GoalContributionModel gcm = new GoalContributionModel(gm.getGoalId(), gm.getUserId(), Math.random() * gm.getTotal() / 10 + gm.getTotal() / 100, new Date());
                msApi.goals().saveContribution(gcm, new IApiListener<GoalContributionModel>() {
                    @Override
                    public void onSuccess(GoalContributionModel goalContributionModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Contribution saved successfully -> " + goalContributionModel.getId(), Toast.LENGTH_LONG).show());
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();
                        activity.runOnUiThread(() -> Toast.makeText(activity, "Contribution Error -> " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());

                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void getAllContributions() {
        msApi.goals().getAllContributions(new IApiListListener<GoalContributionModel>() {
            @Override
            public void onSuccess(@NotNull List<? extends GoalContributionModel> list) {
                //Singletons.getInstance().getNetworkCallQueue().decrement();

                activity.runOnUiThread(() -> {
                    String[] dialogItems = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        GoalContributionModel cm = list.get(i);
                        dialogItems[i] = String.format(
                                Locale.getDefault(),
                                "CID=%d, GID=%d, $%.2f, %%%.1f T=%d",
                                cm.getId(),
                                cm.getGoalId(),
                                cm.getContributionAmount(),
                                cm.getPercent(),
                                cm.getContributionTypeId().getValue());
                    }
                    new AlertDialog
                            .Builder(activity)
                            .setTitle("AllContributions")
                            .setItems(dialogItems, (dialog1, which1) -> {
                            })
                            .create()
                            .show();
                });
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                //Singletons.getInstance().getNetworkCallQueue().decrement();

                activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                getAll(false, null);
            }
        });
    }

    private void getAll(boolean showDialog, DialogInterface.OnClickListener listener) {
        msApi.goals().getAll(new IApiListListener<GoalModel>() {
            @Override
            public void onSuccess(@NotNull List<? extends GoalModel> list) {
                goals = new ArrayList<>(list);
                if (showDialog) {
                    showGoalsListDialog(listener);
                }
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                activity.runOnUiThread(() -> Toast.makeText(activity, "Error: " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
            }
        });
    }

    private void deleteGoal() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                msApi.goals().deleteGoal(gm.getGoalId(), new IApiListener<ApiResponseModel>() {
                    @Override
                    public void onSuccess(ApiResponseModel goalModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Goal " + gm.getGoalId() + " is deleted successfully", Toast.LENGTH_LONG).show());
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void getGoal() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                msApi.goals().getGoal(gm.getGoalId(), new IApiListener<GoalModel>() {
                    @Override
                    public void onSuccess(GoalModel goalModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        showGoalDetailsDialog(goalModel);
                        goals.set(which, goalModel);
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void saveManualContribution() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                msApi.goals().saveManualContribution(gm.getGoalId(), Math.random() * gm.getTotal() / 10 + gm.getTotal() / 20, new IApiListener<Void>() {
                    @Override
                    public void onSuccess(Void v) {

                        //Singletons.getInstance().getNetworkCallQueue().decrement();
                        activity.runOnUiThread(() -> Toast.makeText(activity, "Contribution successfully saved", Toast.LENGTH_LONG).show());
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void unlinkTransaction() {

    }

    private void updateGoal() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                gm.setDescription("Updated description 45");
                gm.setTitle("Somet 19");
                gm.setStartBalance(685.0);
                gm.setTotal(9859.0);
                msApi.goals().updateGoal(gm, new IApiListener<GoalModel>() {
                    @Override
                    public void onSuccess(GoalModel goalModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Update Successful", Toast.LENGTH_LONG).show());
                        showGoalDetailsDialog(goalModel);
                        goals.set(which, goalModel);
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void saveGoal() {
        Calendar calendar = GregorianCalendar.getInstance();
        Date startDate = calendar.getTime();
        calendar.add(Calendar.MONTH, 14);
        Date endDate = calendar.getTime();
        GoalModel gm = new GoalModel(GoalType.Holiday, "new life 15", "something new 16", (Math.random() * 40000.0 + 30000.0), startDate, endDate, false, 0.50);
        //Singletons.getInstance().getNetworkCallQueue().increment();
        msApi.goals().saveGoal(gm, new IApiListener<GoalModel>() {
            @Override
            public void onSuccess(GoalModel goalModel) {

                //Singletons.getInstance().getNetworkCallQueue().decrement();
                showGoalDetailsDialog(goalModel);
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                //Singletons.getInstance().getNetworkCallQueue().decrement();

                activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
            }
        });
    }

    private void linkTransaction() {
        int[] testTransactionIds = new int[]{999432, 999436, 999437, 999438, 999483, 999484, 1570510, 1570571, 1570634, 1570635, 1570638, 1570639, 1641438, 1641439};
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                msApi.goals().linkTransaction(goals.get(which).getGoalId(), testTransactionIds[new Random().nextInt(testTransactionIds.length)], new IApiListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        activity.runOnUiThread(() -> Toast.makeText(activity, "linkTransaction Successful", Toast.LENGTH_LONG).show());
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());

                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);

    }

    private void getGoalSummary() {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GoalModel gm = goals.get(which);
                //Singletons.getInstance().getNetworkCallQueue().increment();
                msApi.goals().getGoalSummary(gm.getGoalId(), new IApiListener<GoalSummaryModel>() {
                    @Override
                    public void onSuccess(GoalSummaryModel sm) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> {
                            new AlertDialog
                                    .Builder(activity)
                                    .setTitle("Goal Summary")
                                    .setMessage(String.format(Locale.getDefault(), "GoalId= %d\n" +
                                                    "Title= %s\n" +
                                                    "Description= %s\n" +
                                                    "StartMonth= %s\n" +
                                                    "EndMonth= %s\n" +
                                                    "GoalType= %s\n" +
                                                    "Total= %.2f\n" +
                                                    "StartBalance= %.2f\n" +
                                                    "RemainingMonths= %d\n" +
                                                    "ImageId= %d\n" +
                                                    "MonthlySummaries= %s\n",
                                            sm.getGoalId(),
                                            sm.getTitle(),
                                            sm.getDescription(),
                                            new SimpleDateFormat("yy/MM/dd", Locale.getDefault()).format(sm.getStartDate()),
                                            new SimpleDateFormat("yy/MM/dd", Locale.getDefault()).format(sm.getEndDate()),
                                            String.valueOf(sm.getGoalType()),
                                            sm.getTotal(),
                                            sm.getStartBalance(),
                                            sm.getRemainingMonths(),
                                            sm.getImageId(),
                                            String.valueOf(sm.getMonthlySummaries())))
                                    .show();
                        });
                    }

                    @Override
                    public void onError(@NotNull ApiErrorModel apiErrorModel) {
                        //Singletons.getInstance().getNetworkCallQueue().decrement();

                        activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
                        getAll(false, null);
                    }
                });
            }
        };
        if (goals.size() == 0)
            getAll(true, listener);
        else
            showGoalsListDialog(listener);
    }

    private void getDefaultGoalImage() {
        msApi.goals().getDefaultGoalImage(GoalType.Gift, new IApiListener<String>() {
            @Override
            public void onSuccess(String s) {
                ImageView iv = new ImageView(activity);
                iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                activity.runOnUiThread(()->{
                    new AlertDialog
                            .Builder(activity)
                            .setTitle("DefaultGoalImage")
                            .setView(iv)
                            .show();
                    Glide.with(iv).load(s).thumbnail(0.1f);
                });
            }

            @Override
            public void onError(@NotNull ApiErrorModel apiErrorModel) {
                activity.runOnUiThread(() -> Toast.makeText(activity, "Error = " + apiErrorModel.getCode(), Toast.LENGTH_LONG).show());
            }
        });
    }

    private void saveGoalImage() {

    }

    private void showGoalsListDialog(DialogInterface.OnClickListener itemClickListener) {
        activity.runOnUiThread(() -> {
            String[] dialogItems = new String[goals.size()];
            for (int i = 0; i < goals.size(); i++) {
                GoalModel gm = goals.get(i);
                dialogItems[i] = String.format(
                        Locale.getDefault(),
                        "%d - %s, %.2f, end %s, %s, %.2f",
                        gm.getGoalId(),
                        gm.getTitle(),
                        gm.getTotal(),
                        new SimpleDateFormat("yy/MM/dd", Locale.getDefault()).format(gm.getEndDate()),
                        gm.isCompleted(),
                        gm.getMonthContribution());
            }
            new AlertDialog
                    .Builder(activity)
                    .setTitle("Goal List")
                    .setItems(dialogItems, itemClickListener)
                    .create()
                    .show();
        });
    }

    private void showGoalDetailsDialog(GoalModel gm) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog
                        .Builder(activity)
                        .setTitle("Goal Details")
                        .setMessage(String.format(Locale.getDefault(),
                                "GoalID= %d\n" +
                                        "MigratedGoalID= %d\n" +
                                        "UserID= %s\n" +
                                        "GoalType= %s\n" +
                                        "Title= %s\n" +
                                        "Description= %s\n" +
                                        "Total= %.2f\n" +
                                        "StartMonth= %s\n" +
                                        "EndMonth= %s\n" +
                                        "ManualBudget= %.2f\n" +
                                        "CustomImageURL= %s\n" +
                                        "IsCompleted= %s\n" +
                                        "Colour= %s\n" +
                                        "TotalContributions= %.2f\n" +
                                        "StartBalance= %.2f\n" +
                                        "LinkedFinancialAccounts= %d\n" +
                                        "TargetSaved= %d\n" +
                                        "ActualSaved= %d\n" +
                                        "GoalContributions= %d\n" +
                                        "AlertDateUTC= %s\n" +
                                        "ImageId= %d\n" +
                                        "MonthContribution= %.2f\n",
                                gm.getGoalId(),
                                gm.getMigratedGoalID(),
                                gm.getUserId(),
                                String.valueOf(gm.getGoalType()),
                                gm.getTitle(),
                                gm.getDescription(),
                                gm.getTotal(),
                                String.valueOf(gm.getStartDate()),
                                String.valueOf(gm.getEndDate()),
                                gm.getManualBudget(),
                                gm.getCustomImageURL(),
                                String.valueOf(gm.isCompleted()),
                                gm.getColour(),
                                gm.getTotalContributions(),
                                gm.getStartBalance(),
                                gm.getLinkedFinancialAccounts() == null ? 0 : gm.getLinkedFinancialAccounts().size(),
                                gm.getTargetSaved() == null ? 0 : gm.getTargetSaved().size(),
                                gm.getActualSaved() == null ? 0 : gm.getActualSaved().size(),
                                gm.getContributions() == null ? 0 : gm.getContributions().size(),
                                String.valueOf(gm.getAlertDateUTC()),
                                gm.getImageId(),
                                gm.getMonthContribution()
                        ))
                        .show();

            }
        });
    }
}
