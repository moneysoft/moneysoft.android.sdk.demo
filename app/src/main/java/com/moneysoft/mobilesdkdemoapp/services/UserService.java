package com.moneysoft.mobilesdkdemoapp.services;

import android.util.Log;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.sdk.api.MoneysoftApi;
import com.moneysoft.sdk.enumerations.financial.Moneysoft.Engagement;
import com.moneysoft.sdk.enumerations.financial.Moneysoft.LiteUserState;
import com.moneysoft.sdk.listeners.interfaces.IApiListener;
import com.moneysoft.sdk.models.ApiResponseModel;
import com.moneysoft.sdk.models.errors.ApiErrorModel;
import com.moneysoft.sdk.models.platform.PlatformFeatureModel;
import com.moneysoft.sdk.models.user.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class UserService implements IService {
	@Override
	public void callEndpoint(ApiEndpoint endpoint) {
		switch (endpoint.getName().toLowerCase()) {
			case "/api/2.0/user":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getUserProfile();
				}

				break;
			case "/api/2.0/user/adviser":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getAdviser();
				}

				break;
			case "/api/2.0/user/all":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getAdvisedUsers();
				}

				break;
			case "/api/2.0/user/state":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getLiteStatus();
				}

				break;
			case "/api/2.0/user/engagement":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getEngagementLevel();
				}

				break;
			case "/api/2.0/user/confirmaccounts":
				if (endpoint.getMethod().equalsIgnoreCase("put")) {
					List<Integer> confirmedAccounts = new ArrayList<>();
					confirmedAccounts.add(-123456);

					confirmAccounts(confirmedAccounts);
				}

				break;
			case "/api/2.0/user/permissions":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getPermissions();
				}

				break;
			case "/api/2.0/user/creditcard":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					getCreditCard();
				}
				else if (endpoint.getMethod().equalsIgnoreCase("post")) {
					saveCreditCard();
				}
				else if (endpoint.getMethod().equalsIgnoreCase("delete")) {
					cancelCreditCard();
				}

				break;
		}
	}

	private void getUserProfile() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<UserModel> profileListener = new IApiListener<UserModel>() {
			@Override
			public void onSuccess(UserModel profile) {
				Log.d("SDK", profile.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getProfile(profileListener);
	}

	private void getAdviser() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<UserModel> adviserListener = new IApiListener<UserModel>() {
			@Override
			public void onSuccess(UserModel adviserProfile) {
				Log.d("SDK", adviserProfile.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getAdviser(adviserListener);
	}

	private void getAdvisedUsers() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<List<UserSummaryModel>> advisedUsersListener = new IApiListener<List<UserSummaryModel>>() {
			@Override
			public void onSuccess(List<UserSummaryModel> advisedUsers) {
				Log.d("SDK", advisedUsers.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getAdvisedUsers(advisedUsersListener);
	}

	private void getLiteStatus() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<LiteUserState> userStateListener = new IApiListener<LiteUserState>() {
			@Override
			public void onSuccess(LiteUserState status) {
				Log.d("SDK", status.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getLiteState(userStateListener);
	}

	private void getEngagementLevel() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<Engagement> engagementListener = new IApiListener<Engagement>() {
			@Override
			public void onSuccess(Engagement engagement) {
				Log.d("SDK", engagement.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getEngagementLevel(engagementListener);
	}

	private void confirmAccounts(List<Integer> confirmedAccounts) {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<Void> confirmAccountsListener = new IApiListener<Void>() {
			@Override
			public void onSuccess(Void o) {
				Log.d("SDK", "Accounts confirmed");
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().confirmAccounts(new AccountConfirmationModel(confirmedAccounts), confirmAccountsListener);
	}

	private void getPermissions() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<List<PlatformFeatureModel>> permissionsListener = new IApiListener<List<PlatformFeatureModel>>() {
			@Override
			public void onSuccess(List<PlatformFeatureModel> permissions) {
				Log.d("SDK", permissions.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getPermissions(permissionsListener);
	}

	private void getCreditCard() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<MaskedCreditCardModel> creditCardListener = new IApiListener<MaskedCreditCardModel>() {
			@Override
			public void onSuccess(MaskedCreditCardModel creditCard) {
				Log.d("SDK", creditCard.toString());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().getCreditCard(creditCardListener);
	}

	private void saveCreditCard() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<ApiResponseModel> creditCardListener = new IApiListener<ApiResponseModel>() {
			@Override
			public void onSuccess(ApiResponseModel response) {
				Log.d("SDK", "Credit card saved: " + response.getSuccess());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		Calendar expiryDate = Calendar.getInstance();
		expiryDate.set(Calendar.YEAR, 2020);
		expiryDate.set(Calendar.MONTH, 12);
		expiryDate.set(Calendar.DATE, 1);
		expiryDate.set(Calendar.HOUR, 0);
		expiryDate.set(Calendar.MINUTE, 0);
		expiryDate.set(Calendar.SECOND, 0);

		msApi.user().saveCreditCard(new CreditCardModel("4444333322221111", "Test Card", expiryDate.getTime(), "123"), creditCardListener);
	}

	private void cancelCreditCard() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<ApiResponseModel> creditCardListener = new IApiListener<ApiResponseModel>() {
			@Override
			public void onSuccess(ApiResponseModel response) {
				Log.d("SDK", "Credit card cancelled: " + response.getSuccess());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				Log.d("SDK.ERROR", error.getCode() + " | " + error.getMessages());
				//Singletons.getInstance().getNetworkCallQueue().decrement();
			}
		};

		msApi.user().cancelCreditCard(creditCardListener);
	}
}
