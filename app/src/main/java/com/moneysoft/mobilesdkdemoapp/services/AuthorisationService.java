package com.moneysoft.mobilesdkdemoapp.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.moneysoft.mobilesdkdemoapp.activities.EndpointActivity;
import com.moneysoft.mobilesdkdemoapp.activities.MainMenuActivity;
import com.moneysoft.mobilesdkdemoapp.models.ApiEndpoint;
import com.moneysoft.sdk.api.MoneysoftApi;
import com.moneysoft.sdk.listeners.interfaces.IApiListener;
import com.moneysoft.sdk.models.errors.ApiErrorModel;
import com.moneysoft.sdk.models.user.AuthenticationModel;
import org.jetbrains.annotations.NotNull;

public class AuthorisationService implements IService {
	private Context context;
	private EndpointActivity activity;

	public AuthorisationService(EndpointActivity context) {
		this.context = context;
		activity = context;
	}

	@Override
	public void callEndpoint(ApiEndpoint endpoint) {
		switch (endpoint.getName().toLowerCase()) {
			case "/token":
				if (endpoint.getMethod().equalsIgnoreCase("post")) {
					login();
				}

				break;
			case "/logout":
				if (endpoint.getMethod().equalsIgnoreCase("get")) {
					logOut();
				}

				break;
		}
	}

	private void login() {
		MoneysoftApi msApi = new MoneysoftApi();
		IApiListener<AuthenticationModel> authListener = new IApiListener<AuthenticationModel>() {
			@Override
			public void onSuccess(AuthenticationModel authentication) {
				activity.hideProgress();
				Log.d("TEST", "Token: " + authentication.getToken());
				Log.d("TEST", "Type: " + authentication.getTokenType());
				Log.d("TEST", "Expiry: " + authentication.getExpiry());
				Log.d("TEST", "Refresh Token: " + authentication.getRefreshToken());

				Intent intent = new Intent(context, MainMenuActivity.class);
				context.startActivity(intent);
			}

			@Override
			public void onError(@NotNull ApiErrorModel error) {
				activity.hideProgress();
				Log.d("TEST", error.getCode() + " | " + error.getMessages());
			}
		};
		activity.showProgress();
		msApi.user().login("msdemo3@moneysoft.com.au", "Moneysoft11", authListener);
	}

	private void logOut() {
		MoneysoftApi msApi = new MoneysoftApi();
		msApi.user().logout();
	}
}
